import React from 'react';
import ReactDOM from 'react-dom';
import Router from './views/router';
import GlobalStyle from './assets/styles/globalStyle'
import 'antd/dist/antd.css'
import { Provider } from 'react-redux'
import store from './store'
import Toastr from './config/toastr'

ReactDOM.render(
  <Provider store={store}>
    <GlobalStyle />
    <Router />
    <Toastr/>
  </Provider>,
  document.getElementById('root')
);
