import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import LayoutBase from '../components/layout'
import PostLayout from '../components/post/item'
import Loading from '../components/loading'
import { getAllPosts, createPost } from '../store/post/post.action'
import {Modal, Button} from 'antd'
import FormInput from '../components/post/form'

const BreadCrumb = ["Home", "Post"]

const Post = () => {
  const [modal, setModal] = useState(false)
  const [update, setUpdate] = useState(false)
  const Actions = <Button onClick={() => setModal(true)}> Novo </Button>
  const dispatch = useDispatch()

  const loading = useSelector((state) => state.post.loading)
  const allPosts = useSelector((state) => state.post.all)

  useEffect(() => {
    dispatch(getAllPosts())
    if(update){
    setUpdate(false)
    }
  }, [dispatch, update])

  const mountPost = () => {

    if (allPosts) {
     return allPosts.map((post, i) => (
        <PostLayout
        key={i}
          author={"futuro autor"}
          avatar={post.avatar}
          title={post.title}
          description={"futura descrição"}
          created_at={post.created_at}
        />
      ));
    }
    return;
  }

  const handleSubmit = (form, e) => {
    e.preventDefault()
    dispatch(createPost(form))
    handleCancel()
    setUpdate(true)
  };
  const handleCancel = () => setModal(false)

  const ModalForm = () => (
    <Modal 
    title="Nova Postagem" 
    visible={modal} 
    footer={false}
    onCancel={handleCancel}>
    <FormInput submit={handleSubmit}/>
  </Modal>
  )

  return (
    <LayoutBase 
    breadcrumb={BreadCrumb} 
    title="Postagens" 
    actions={Actions}
    >
      
      <ModalForm/>
      {loading ? 
      <Loading /> 
      : mountPost()}

    </LayoutBase>
  );
}

export default Post;


