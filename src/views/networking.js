import LayoutBase from '../components/layout'
import { Collapse } from 'antd';
import styled from 'styled-components'
import Cards from '../components/networking/cards'


const { Panel } = Collapse;
const BreadCrumb = ["Home", "Minha Rede"]

const Networking = () => {
  const Actions = () => ""
  const mockCards = [...Array(10).keys()]


  return (
    <LayoutBase breadcrumb={BreadCrumb}
      title="Minha Rede"
      actions={Actions()}>

      <Collapse accordion>
        <StyledPanel header="AMIGOS" key="1">
          <CardBox>          
          {mockCards.map((v, i) => (
            <Cards key={i}/>
          ))}            
          </CardBox>
        </StyledPanel>
        <StyledPanel header="SUGESTÕES DE NOVAS AMIZADES" key="2">
        <CardBox>
        {mockCards.map((v, i) => (
            <Cards key={i}/>
          ))}   
          </CardBox>
        </StyledPanel>
      </Collapse>

    </LayoutBase>
  );
}

export default Networking;

const CardBox = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 290px));
  grid-auto-rows: auto;
  grid-gap: 1rem;
  max-height: 700px;
  overflow-y: auto;  
`

const StyledPanel = styled(Panel)`
  .ant-collapse-header{
    background-color: #001529;
    color: #fff !important;
    margin: 10px auto;
  }
`