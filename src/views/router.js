import { Route, Router, Switch, Redirect } from 'react-router-dom'
import history from './../config/history'
import { isAuthenticated } from '../config/auth'

//Views
import Post from './post'
import Profile from './profile'
import Panel from './panel'
import Networking from './networking'
import SignIn from './Sign/signin'


const AuthRoute = ({ ...rest }) => {
    if (!isAuthenticated()) {
        return <Redirect to='/signin' />
    }
    return <Route {...rest} />
}

const Routers = () => (
    <Router history={history}>
        <Switch>
        <AuthRoute exact path="/perfil" component={Profile} />
        <AuthRoute exact path="/painel" component={Panel} />
        <AuthRoute exact path="/minharede" component={Networking} />
        <Route exact path="/signin" component={SignIn} />
        <AuthRoute exact path="/" component={Post} />        
        </Switch>
    </Router>
)

export default Routers;