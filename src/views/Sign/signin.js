import { useState } from 'react'
import { Layout, Col, Form, Input, Button } from 'antd'
import { useDispatch } from 'react-redux';
import styled from 'styled-components'
import { signIn } from '../../store/sign/action.sign'

const { Content } = Layout


const SignIn = () => {

    const dispatch = useDispatch()
    const [form, setForm] = useState({})
    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
    }

    const submitForm = () => {
        dispatch(signIn(form));
    }

    return (
        <Layout className="layout">
            <Main>
                <SliceBg span={16}>
                    <BgImg />
                     DEV CONNECT
                </SliceBg>
                <SliceForm span={8}>
                    <FormLogin>
                        <Form
                            initialValues={{
                                ...form
                            }}
                        >
                            <Form.Item
                                name="email"
                                rules={[{
                                    required: true,
                                    message: 'Please input your email!'
                                }]}
                            >
                                <Input
                                    name="email"
                                    value={form.email || ""}
                                    onChange={handleChange}
                                    placeholder="email@example.com"
                                />
                            </Form.Item>

                            <Form.Item
                                name="password"
                                rules={[{
                                    required: true,
                                    message: 'Please input your password!'
                                }]}
                            >
                                <Input.Password
                                    name="password"
                                    value={form.password || ""}
                                    onChange={handleChange}
                                    placeholder="Password"
                                />
                            </Form.Item>

                            <Form.Item >
                                <Button
                                    onClick={submitForm}
                                    type="primary"
                                    htmlType="submit"
                                >
                                    Submit
        </Button>
                            </Form.Item>
                        </Form>
                    </FormLogin>
                </SliceForm>
            </Main>
        </Layout>
    );

};

export default SignIn

const Main = styled(Content)`
display: flex;
height: 100vh;
`
const SliceBg = styled(Col)`
height: 100vh;
display: flex;
position: relative;
flex-direction: column;
justify-content: center;
align-items: center;
font-size: 5rem;
font-weight: 700;
color: #061b35;
text-shadow: 4px 4px 2px #43949e;
line-height: 1;
`
const BgImg = styled.div``

const SliceForm = styled(Col)`
height: 100vh;
  display: flex;
  background-image: linear-gradient(180deg, #1c516a, #061b35);
  padding: 20px;
`
const FormLogin = styled.div`
  padding: 20px;
  width: 100%;
  align-self: center;
`