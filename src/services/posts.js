import http from '../config/http'

const getPostService = () => {
    return http.get('/topic')
}

const createPostService = async (data) => {
    return await http.post(`/topic`, data)
}

export { getPostService, createPostService }