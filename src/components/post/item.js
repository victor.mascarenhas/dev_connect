import { Comment } from 'antd'
import styled from 'styled-components'
  
 const Post = ({author, avatar, title, description, created_at}) => {
   
    return (      
     <StyledPost
      author={author}
      avatar={
        <img
          src={avatar || "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"}
          alt={title}
        />
      }
      content={
        <>
        <h4>{title}</h4>
        <p>
         {description}
        </p>
        </>
      }
      datetime={created_at}
    />
    );
  }

  export default Post;
  

  const StyledPost = styled(Comment)`
  background-image: linear-gradient(360deg, #fff, #eee);
  border: thin solid #eee;
  margin-bottom: 10px;
  .ant-comment-avatar img{
    width: 80px;
    height: 80px;
    margin: 10px;
  }
  h4{
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  }
  `
  