import { POST_LOADING, GET_POSTS, CREATE_POST } from './post.action'

const INITIAL_STATE = {
    all: [],
    loading: false
}

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_POSTS:
            state.all = action.data;
            state.loading = false
            return state
        case POST_LOADING:
            state.loading = action.status;
            return state;
        case CREATE_POST:
            state.loading = false;
            return state;

        default:
            return state;
    }
}

export default reducer