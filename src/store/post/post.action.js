import { getPostService, createPostService } from "../../services/posts";
import { toastr } from 'react-redux-toastr'

export const POST_LOADING = "POST_LOADING";
export const GET_POSTS = "GET_POSTS";
export const CREATE_POST = "CREATE_POST";

export const getAllPosts = () => {
    return async (dispatch) => {
        dispatch({ type: POST_LOADING, status: true })
        const posts = await getPostService()
        dispatch({ type: GET_POSTS, data: posts.data })
    }
};

export const createPost = (form) => {
    return async (dispatch) => {
        const post = {
            //author = form.author
            //avatar = form.avatar
            title : form.title,
            //description : form.description
            //created_at = form.created_at
        }
        dispatch({ type: POST_LOADING, status: true })
        try{
            const res = await createPostService(post)
            if(res.data){
            dispatch({ type: CREATE_POST, post})
            toastr.success('SUCESSO!', 'Postagem criada com sucesso!')
            getAllPosts()
            }
        }       
        catch(error){
            toastr.error('ERRO!', 'Houve um problema ao criar a postagem!')                   
        }
    }
} 
