import { saveLocalStorage } from "../../config/auth";
import  authService  from '../../services/auth'
import history from '../../config/history'

export const SIGN = "SIGN";
export const SIGN_LOADING = "SIGN_LOADING";

export const signIn = (props) => {
    return async (dispatch) => {    
        try{
    dispatch({ type: SIGN_LOADING, loading: true})
    const { data } = await authService(props)
    dispatch({ type: SIGN, data: data})
    saveLocalStorage(data)
    history.push('/')
        }catch(error){
            console.log(error.message)
        }
    }
};

