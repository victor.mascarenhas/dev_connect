import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import {reducer as toastrReducer} from 'react-redux-toastr'
import thunk from 'redux-thunk'
import multi from 'redux-multi'

import SignReducer from './sign/reducer.sign'
import PostReducer from './post/post.reducer'

const reducers = combineReducers({
    auth: SignReducer,
    post: PostReducer,
    toastr: toastrReducer,
});

const middleware = [thunk, multi];

const compose = composeWithDevTools(applyMiddleware(...middleware));

const store = createStore(reducers, compose);

export default store;